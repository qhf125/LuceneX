# LuceneX

#### 项目介绍
国行版全文检索
#### 软件架构
- lucene      ---> 全文检索开发包
- threadPool  ---> java线程池
- log         ---> java自带logger
- 分词器       ---> IK 优化版
#### 安装教程
最新版:下载源码 执行 mvn clean package
/去maven中央仓库获取
#### 集成/启动教程  没启动LuceneX 无法使用 service
###### main 启动
LuceneX.start(DemoLuceneXConfig.class);

###### Jfinal
插件添加 JfinalLuceneX
###### springboot
自定义filter 过滤器 WebLuceneX
参考
```
<filter>
     <filter-name>luceneX</filter-name>
     <filter-class>com.lucenex.core.plugins.WebLuceneX</filter-class>
     <init-param>
         <param-name>configClass</param-name>
         <param-value>com.demo.common.DemoLuceneXConfig</param-value>
     </init-param>
 </filter>

```
#### 使用说明
继承 CoreConfig 实现内置方法
1. 新增索引源
```
@Override
 public void configLuceneX(LuceneX luceneX){
     luceneX.add("d:/lucene/", "test1");
     //多个
     luceneX.add("d:/lucene/", "test2");
     luceneX.add("d:/lucene/", "test3");
     luceneX.add("d:/lucene/", "test4");
 }

```


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### LuceneX 特点

1. 0配置开箱即用
1. 内置线程池支持添加索引无需等待
1. 内置丰富的常用方法-帮助快速开发
1. 自带垃圾回收机制-无需担心资源泄露
1. 可插拔式插件设计、词库、高亮
1. 自带中文分词器无需集成
1. 基于lucene薄封装-快速开发同时、不牺牲性能
1. 内置读写索引转换 实时检索不含糊
1. 注解式字段声明、灵活可变
1. MVC 设计模式

#### 捐赠
觉得本项目对您有所帮助、可以请作者喝杯小茶、抽根好烟
![输入图片说明](https://gitee.com/uploads/images/2018/0504/010352_526bfb6e_440716.jpeg "1525366610796.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0504/010455_ba2dad54_440716.png "weixin.png")


#### QQ群
475349334