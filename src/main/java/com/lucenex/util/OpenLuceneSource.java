package com.lucenex.util;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.MultiReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.store.NoLockFactory;

import com.ld.ThreadPool.LoggerFactory;
import com.lucenex.core.LuceneDataSource;
import com.lucenex.core.LuceneXConfig;
/**
 * Lucene资源管理
 * @author zxw
 *
 */

public class OpenLuceneSource {


	private static  Logger logger = LoggerFactory.getLogger(OpenLuceneSource.class);

	/**
	 * 打开写源
	 * @return
	 * @throws IOException
	 */
	public static IndexWriter indexWriter(String indexPath,PerFieldAnalyzerWrapper analyzer) throws IOException {
		Path path = Paths.get(indexPath);
		if(!Files.isReadable(path)) {
			throw new IOException("文件目录不可读或不存在:"+indexPath);
		}
		FSDirectory dir = NIOFSDirectory.open(path, NoLockFactory.INSTANCE);
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		IndexWriter indexWriter = new IndexWriter(dir, indexWriterConfig);
		return indexWriter;
	}

	/**
	 * 打开读源
	 * @return
	 * @throws IOException
	 */
	public static IndexSearcher indexSearcher(IndexWriter indexWriter) throws IOException {
		DirectoryReader reader = DirectoryReader.open(indexWriter); 
		IndexSearcher indexSearcher = new IndexSearcher(reader);
		return indexSearcher;
	}

	/**
	 * 写源 转换到 读源
	 * @param indexWriter
	 * @return
	 * @throws IOException
	 */
	public static IndexSearcher dynamicIndexSearcher(IndexWriter indexWriter) throws IOException {
		DirectoryReader reader = DirectoryReader.open(indexWriter); 
		IndexSearcher indexSearcher = new IndexSearcher(reader);
		return indexSearcher;
	}

	/**
	 * 多源合并 查询
	 * @param str
	 * @return
	 * @throws IOException
	 */
	public static LuceneXConfig mergeSource(String... str){
		IndexReader[] indexReaders = new IndexReader[str.length];
		for (int i = 0; i < str.length; i++) {
			IndexWriter writer = LuceneDataSource.build().getDataSource(str[i]).getWriter();
			try {
				indexReaders[i] = DirectoryReader.open(writer);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "openLuceneSource mergeSource indexWriter error", e);
			}
		}
		MultiReader reader = null;
		try {
			reader = new MultiReader(indexReaders);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "openLuceneSource mergeSource MultiReader error", e);
		}
		IndexSearcher indexSearcher = new IndexSearcher(reader);
		LuceneXConfig luceneXConfig = new LuceneXConfig();
		luceneXConfig.setSearcher(indexSearcher);
		return luceneXConfig;
	}


}
