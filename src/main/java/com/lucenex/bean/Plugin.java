package com.lucenex.bean;

import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;

/**
 * 插件接口
 * @author zxw
 *
 */
public interface Plugin{
	
	Dic dic();
	Highlighter highlighter(QueryScorer scorer);

}
