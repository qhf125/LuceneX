package com.lucenex.service;



import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;

import com.lucenex.util.Page;

/**
 * 接口
 * @author zxw
 *
 */
public interface LuceneService {

	/**
	 * 添加索引集合
	 * @param obj 原声 javaBean
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void saveObj(List<T> object);
	
	/**
	 * 添加索引对象
	 * @param obj 原声 javaBean
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void saveObj(T object);
	
	/**
	 * 添加索引集合
	 * @param map 原声 javaBean
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void saveMap(List<T> map,Class<?> clas);
	
	/**
	 * 添加索引对象
	 * @param map 原声 javaBean
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void saveMap(T map,Class<?> clas);
	
	/**
	 * 添加索引集合
	 * @param documents
	 * @throws IOException
	 */
	<T> void saveDocument(List<Document> document);
	/**
	 * 添加索引对象
	 * @param document
	 * @throws IOException
	 */
	<T> void saveDocument(Document document);

	/**
	 * 清空索引
	 * @throws IOException
	 */
	void delAll() throws IOException;

	/**
	 * 删除索引
	 * @param term
	 * @throws IOException
	 */
	void delKey(Term term) throws IOException;

	/**
	 * 删除索引
	 * @param query
	 * @throws IOException
	 */
	void delKey(Query query) throws IOException;

	/**
	 * 更新索引集合
	 * @param objs obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateObj(List<T> object, Term term);
	/**
	 * 更新索引对象
	 * @param obj obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateObj(T object, Term term);
	/**
	 * 更新索引集合
	 * @param objs obj 原声 javaBean
	 * @param query
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateObj(List<T> object, Query query);
	/**
	 * 更新索引对象
	 * @param obj obj 原声 javaBean
	 * @param query
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateObj(T object, Query query);
	/**
	 * 更新索引集合
	 * @param objs obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateMap(List<T> map, Term term,Class<?> clas);
	/**
	 * 更新索引对象
	 * @param obj obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateMap(T map, Term term,Class<?> clas);
	/**
	 * 更新索引集合
	 * @param objs obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateMap(List<T> map, Query query,Class<?> clas);
	/**
	 * 更新索引对象
	 * @param obj obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	<T> void updateMap(T map, Query query,Class<?> clas);
	
	/**
	 * 更新索引集合
	 * @param docs
	 * @param term
	 * @throws IOException
	 */
	void updateDocument(List<Document> document, Term term);
	/**
	 * 更新索引对象
	 * @param doc
	 * @param term
	 * @throws IOException
	 */
	void updateDocument(Document document, Term term);
	/**
	 * 更新索引集合
	 * @param docs
	 * @param term
	 * @
	 */
	void updateDocument(List<Document> document, Query query);
	/**
	 * 更新索引对象
	 * @param doc
	 * @param term
	 * @
	 */
	void updateDocument(Document document, Query query);

	/**
	 * 查询索引集合
	 * @param query
	 * @param obj 原声 javaBean class
	 * @param num 条数 0 为所有
	 * @param sort 可以为 null
	 * @return
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	<T> List<T> findList(Query query, Class<T> clas, int num, Sort sort) throws IOException, InvalidTokenOffsetsException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, IllegalArgumentException, InvocationTargetException;

	/**
	 * 查询对象集合分页
	 * @param query
	 * @param pageNumber 当前页  从 1开始
	 * @param pageSize 每页条数 0 为 10
	 * @param obj 原声 javaBean class
	 * @param sort 可以为 null
	 * @return
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	<T> Page<T> findList(Query query, int pageNumber, int pageSize, Class<T> clas, Sort sort) throws IOException, InvalidTokenOffsetsException, InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, IllegalArgumentException, InvocationTargetException;

	
	
}
