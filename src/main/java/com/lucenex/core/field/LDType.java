package com.lucenex.core.field;

/**
 * 字段类型
 * @author zxw
 *
 */
public enum LDType {
	
	TextField,
	StringField,
	DateField,
	IntPoint,
	LongPoint,
	FloatPoint,
	DoublePoint,
	BinaryPoint
}
