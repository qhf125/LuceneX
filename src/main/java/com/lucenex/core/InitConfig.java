package com.lucenex.core;

import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;

public interface InitConfig {
	
	void add(String indexPath,String dataKey);
	void add(String indexPath,String dataKey,boolean highlight);
	void add(String indexPath,String dataKey,boolean highlight, PerFieldAnalyzerWrapper analyzer);
	void createSource(String indexPath,String dataKey,boolean highlight,PerFieldAnalyzerWrapper analyzer);

}
