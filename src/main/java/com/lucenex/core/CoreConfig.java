package com.lucenex.core;



import java.io.IOException;

import com.lucenex.bean.BeanConfig;
/**
 * 底层配置类
 * @author zxw
 *
 */
public abstract class CoreConfig {
	
	/**
	 * 插件管理
	 * @param bean
	 */
	public void configPlugin(BeanConfig bean) {
		//默认插件
	};
	
	/**
	 * 核心配置
	 * @param luceneX
	 * @throws IOException
	 */
	public abstract void configLuceneX(LuceneX luceneX);

}
