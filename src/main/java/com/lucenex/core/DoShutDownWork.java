package com.lucenex.core;


import java.util.logging.Logger;

import com.ld.ThreadPool.LoggerFactory;
import com.lucenex.bean.BeanConfig;


/**
 * 异常关闭、手动关闭 处理工作
 * @author zxw
 *
 */
public class DoShutDownWork {
	
	private static Logger log = LoggerFactory.getLogger(DoShutDownWork.class);

	public synchronized static void build() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				log.info("程序退出-开始执行清理工作");
				LuceneDataSource.build().close();
				log.info("调用JAVA垃圾回收");
				System.gc();
			}
		});
	}

}
