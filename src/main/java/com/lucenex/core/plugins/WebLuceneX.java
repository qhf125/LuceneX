package com.lucenex.core.plugins;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.lucenex.core.LuceneX;
/**
 * WEB 通用集成方式
 * @author zxw
 *
 */
public class WebLuceneX implements Filter{

	@Override
	public void destroy() {
		try {
			LuceneX.submit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		arg2.doFilter(arg0, arg1);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String configClass = filterConfig.getInitParameter("configClass");
		try {
			LuceneX.start(configClass);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IOException e) {
			throw new RuntimeException("Please set configClass parameter of WebLuceneX in web.xml");
		}
	}
}
