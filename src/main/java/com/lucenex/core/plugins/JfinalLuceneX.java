package com.lucenex.core.plugins;



import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jfinal.plugin.IPlugin;
import com.ld.ThreadPool.LoggerFactory;
import com.lucenex.core.LuceneX;

/**
 * jfinal 插件
 * @author zxw
 *
 */
public class JfinalLuceneX implements IPlugin{
	
	private Logger log = LoggerFactory.getLogger(JfinalLuceneX.class);
	
	Class<?> coreConfig;
	
	@SuppressWarnings("unused")
	private JfinalLuceneX() {
	}
	
	public JfinalLuceneX(Class<?> coreConfig) {
		this.coreConfig = coreConfig;
	}

	@Override
	public boolean start() {
		if(this.coreConfig != null) {
			try {
				LuceneX.start(this.coreConfig);
				return true;
			} catch (InstantiationException | IllegalAccessException | IOException e) {
				log.log(Level.SEVERE, "jfinalLuceneX start error", e);
				return false;
			}
		}else {
			return false;
		}
	}

	@Override
	public boolean stop() {
		//有自动回收站  无需管理
		return false;
	}

}
