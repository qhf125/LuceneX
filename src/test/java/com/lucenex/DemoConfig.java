package com.lucenex;

import com.lucenex.bean.BeanConfig;
import com.lucenex.core.CoreConfig;
import com.lucenex.core.LuceneX;

public class DemoConfig extends CoreConfig{

	@Override
	public void configPlugin(BeanConfig bean) {
	}

	@Override
	public void configLuceneX(LuceneX luceneX){
		luceneX.add("/Users/zxw/Desktop/work/lucene/", "test");
	}
}
