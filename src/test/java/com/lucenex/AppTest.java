package com.lucenex;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.lucene.document.Document;

import com.ld.ThreadPool.LdThreadPool;
import com.ld.ThreadPool.core.LdThreadPoolFactory;
import com.lucenex.service.LuceneService;
import com.lucenex.util.ObjectKit;


/**
 * Unit test for simple App.
 */
public class AppTest {
	static LuceneService service;
	public static void main(String[] args) throws Exception {
		List<UserDto> list = add();
		List<Document> toDocs = ObjectKit.objToDocs(list);
		for (int j = 0; j < 1; j++) {
			System.out.println("开始时间"+LocalDateTime.now());
			int size = toDocs.size();
			List<UserDto> listObj = new ArrayList<>();
			CountDownLatch countDownLatch = new CountDownLatch(size);
			LdThreadPoolFactory poolFactory = LdThreadPool.build().get();
			for (int i = 0; i < size; i++) {
				Document document = toDocs.get(i);
				listObj.add(ObjectKit.object(toDocs.get(i), UserDto.class));
//				poolFactory.execute(()->{
//						listObj.add(build.object(document, UserDto.class));
//						countDownLatch.countDown();
//				});
			}
//			countDownLatch.await();
			LocalDateTime dateTime = LocalDateTime.now();
			LocalTime localTime = dateTime.toLocalTime();
			System.out.println("结束时间"+LocalDateTime.now());
		}
	}
	
	public static List<UserDto> add() {
		List<UserDto> list = new ArrayList<>();
		for (int i = 0; i < 1000000; i++) {
			UserDto userDto = new UserDto();
			userDto.setId(i);
			userDto.setName("习近平同印度总理莫迪共同参观精品文物展");
			userDto.setRedm("　新华网北京4月27日电 27日下午，国家主席习近平同来华进行非正式会晤的印度总理莫迪在湖北省博物馆参观精品文物展。两国领导人一同欣赏具有悠久历史的中华文明、特别是荆楚文化，并就加强中印两个文明古国交流互鉴、推动不同文明和谐共处和对话交换意见");
			list.add(userDto);
		}
		return list;
	}
}
