package com.lucenex;

import java.io.Serializable;

import com.lucenex.core.field.FieldKey;
import com.lucenex.core.field.LDType;

public class UserDto implements Serializable{
	
	private static final long serialVersionUID = -5826275601899023160L;
	
	
	
	@FieldKey(type=LDType.IntPoint)
	private int id;
	@FieldKey(type=LDType.StringField)
	private String name;
	@FieldKey(type=LDType.TextField)
	private String redm;
	@Override
	public String toString() {
		return "UserDto [id=" + id + ", name=" + name + ", redm=" + redm + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRedm() {
		return redm;
	}
	public void setRedm(String redm) {
		this.redm = redm;
	}
	
	

	
}
